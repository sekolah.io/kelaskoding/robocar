/***
 * Kelas Koding Coach Salleh
 * 
 * MyRoboCar v1.0
 * 
 * Arduino code for controlling a car robot
 * 
 * Robot Hardware:
 * Main Arduino Board: Arduino UNO or equivalent (e.g. Cytron Maker UNO - https://www.cytron.io/p-maker-uno-simplifying-arduino-for-education)
 * Motor Driver: Cytron Maker Drive - https://www.cytron.io/p-maker-drive
 *
 */

 /***
 * Bahagian A: load kod dari library lain
 */
 #include "CytronMotorDriver.h"


/***
 * Bahagian B: penetapan pembolehubah dan pemalar
 */

// pin motor
#define pinMotor1A  5
#define pinMotor1B  6
#define pinMotor2A  9
#define pinMotor2B  10

// Configure the motor driver.
CytronMD motorKanan(PWM_PWM, pinMotor1A, pinMotor1B);   // PWM 1A = Pin 5, PWM 1B = Pin 6.
CytronMD motorKiri(PWM_PWM, pinMotor2A, pinMotor2B);   // PWM 2A = Pin 9, PWM 2B = Pin 10.


/***
 * Bahagian C: kod program utama
 */
 
void setup() {
  // kod dalam blok ini hanya akan berjalan sekali setiap kali sistem bermula/reset

}

void loop() {
  // kod dalam blok ini akan berjalan berulang-ulang
  pusingKanan(128);
  delay(500);

  kehadapan(200);
  delay(1000);
  
  pusingKiri(128);
  delay(1000);

  kebelakang(200);
  delay(1000);

  berhenti();
  delay(1000);
}

/***
 * Bahagian D: kod program sampingan yang digunakan oleh kod program utama
 */

void pusingKanan(int kelajuan) {
  motorKanan.setSpeed(-kelajuan);
  motorKiri.setSpeed(kelajuan);
}

void pusingKiri(int kelajuan) {
  motorKanan.setSpeed(kelajuan);
  motorKiri.setSpeed(-kelajuan);
}

void kehadapan(int kelajuan) {
  motorKanan.setSpeed(kelajuan);
  motorKiri.setSpeed(kelajuan);
}

void kebelakang(int kelajuan) {
  motorKanan.setSpeed(-kelajuan);
  motorKiri.setSpeed(-kelajuan);
}

void berhenti() {
  motorKanan.setSpeed(0);
  motorKiri.setSpeed(0);
}
