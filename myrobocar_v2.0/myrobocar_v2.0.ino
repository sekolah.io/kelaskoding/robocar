/***
 * Kelas Koding Coach Salleh
 * 
 * MyRoboCar v2.0
 * 
 * Arduino code for controlling a car robot
 * 
 * Robot Hardware:
 * Main Arduino Board: Arduino UNO or equivalent (e.g. Cytron Maker UNO - https://www.cytron.io/p-maker-uno-simplifying-arduino-for-education)
 * Motor Driver: Cytron Maker Drive - https://www.cytron.io/p-maker-drive
 *
 */

 /***
 * Bahagian A: load kod dari library lain
 */
 #include "CytronMotorDriver.h"
 #include "pitches.h"


/***
 * Bahagian B: penetapan pembolehubah dan pemalar
 */

//pemalar untuk pengiraan
#define masaPusingan360 1000
#define masaRalatMaksimum 200
#define kelajuanBerpusing 200
#define kelajuanBergerak 200
#define jarakSesaat 10

// pin motor
#define pinMotor1A  5
#define pinMotor1B  6
#define pinMotor2A  9
#define pinMotor2B  10

// Configure the motor driver.
CytronMD motorKanan(PWM_PWM, pinMotor1A, pinMotor1B);   // PWM 1A = Pin 5, PWM 1B = Pin 6.
CytronMD motorKiri(PWM_PWM, pinMotor2A, pinMotor2B);   // PWM 2A = Pin 9, PWM 2B = Pin 10.

// pin Speaker
#define pinSpeaker 8

// note melodi
int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// jangkamasa note: 4 = quarter note, 8 = eighth note
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

/***
 * Bahagian C: kod program utama
 */
 
void setup() {
  // kod dalam blok ini hanya akan berjalan sekali setiap kali sistem bermula/reset
  delay(500);
  mainkanMelodi();
  
  // tunggu 2 saat sebelum bergerak
  delay(2000);
}

void loop() {
  // kod dalam blok ini akan berjalan berulang-ulang

  //gerak ke hadapan 50cm
  kehadapanBerjarak(50);
  berhenti(2.0);

  //pusing kekanan 90 darjah
  pusingKananBerdarjah(90);
  berhenti(2.0);

  //pusing kekiri 90 darjah
  pusingKiriBerdarjah(90);
  berhenti(2.0);

  //gerak kebelakang 50cm
  kebelakangBerjarak(50);
  berhenti(2.0);
}

/***
 * Bahagian D: kod program sampingan yang digunakan oleh kod program utama
 */

//pusingkan RoboCar kekanan, kelajuan mestilah antara 0 - 255
void pusingKanan(int kelajuan) {
  motorKanan.setSpeed(-kelajuan);
  motorKiri.setSpeed(kelajuan);
}

//pusingkan RoboCar kekiri, kelajuan mestilah antara 0 - 255
void pusingKiri(int kelajuan) {
  motorKanan.setSpeed(kelajuan);
  motorKiri.setSpeed(-kelajuan);
}

//gerakkan RoboCar kehadapan, kelajuan mestilah antara 0 - 255
void kehadapan(int kelajuan) {
  motorKanan.setSpeed(kelajuan);
  motorKiri.setSpeed(kelajuan);
}

//gerakkan RoboCar kebelakang, kelajuan mestilah antara 0 - 255
void kebelakang(int kelajuan) {
  motorKanan.setSpeed(-kelajuan);
  motorKiri.setSpeed(-kelajuan);
}

//hentikan RoboCar
void berhenti() {
  motorKanan.setSpeed(0);
  motorKiri.setSpeed(0);
}

//hentikan RoboCar selama jumlah saat yang diberikan
void berhenti(float saat) {
  motorKanan.setSpeed(0);
  motorKiri.setSpeed(0);
  int masaDalamMillisecond  = saat * 1000;

  delay(masaDalamMillisecond);
}

//pusingkan RoboCar kekanan mengikut jumlah darjah pusingan yang diberikan.
//RoboCar akan berpusing dengan kelajuan mengikut pemalar 'kelajuanBerpusing'
void pusingKananBerdarjah(int darjah) {
  int durasi = map(darjah, 0, 360, 0, masaPusingan360);
  int masaRalat = map(360 - darjah, 0, 360, masaRalatMaksimum, 0); 

  pusingKanan(kelajuanBerpusing);
  delay(durasi + masaRalat);
}

//pusingkan RoboCar kekiri mengikut jumlah darjah pusingan yang diberikan.
//RoboCar akan berpusing dengan kelajuan mengikut pemalar 'kelajuanBerpusing'
void pusingKiriBerdarjah(int darjah) {
  int durasi = map(darjah, 0, 360, 0, masaPusingan360);
  int masaRalat = map(360 - darjah, 0, 360, masaRalatMaksimum, 0);

  pusingKiri(kelajuanBerpusing);
  delay(durasi + masaRalat);
}

void kehadapanBerjarak(int jarak) {
  float masaSatuCM = jarakSesaat/1000;

  kehadapan(kelajuanBergerak);
  delay(masaSatuCM * jarak);
}

void kebelakangBerjarak(float jarak) {
  float masaSatuCM = 1000/jarakSesaat;

  kebelakang(kelajuanBergerak);
  delay(masaSatuCM * jarak);
}

void mainkanMelodi() {
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(pinSpeaker, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(pinSpeaker);
  }
}
